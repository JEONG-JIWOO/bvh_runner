//#include <wiringPi.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>

#include <random>
#include <unistd.h>
#include <iterator>
#include <iostream>
#include <math.h>     
#include <vector>
#include <fstream>
#include <string>

#include <ratio>

#include <boost/lexical_cast.hpp>
#include <dirent.h>
#include <cstdio>

#include <pthread.h>
#include <chrono>
#include <future>
#include <thread>
//#include <ros/ros.h>
//#include "std_msgs/String.h"

using namespace std;
using namespace std::chrono_literals;

static inline std::string &rtrim(std::string &s) // �ڿ� �ִ� ���� ���� ���� �Լ�
{
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

class Bvh // .trj ������ �о �����ϴ� Ŭ����
{
private:

	vector<int> split_double(string str)  // ���ڿ��� �о double vector �� �������ִ� �Լ�
	{
		
		istringstream iss(str);
		vector<string> buffer{ istream_iterator<string>{iss},istream_iterator<string>{} };
		vector<double> res_xyz;
		vector<int> res;

		for (unsigned int i = 0; i < buffer.size(); i++)  // ���۸� �����鼭 double�� ��ȯ
		{
			try
			{	
				double d =0;
				stringstream(buffer[i]) >> d;
				res_xyz.push_back(d);
			}
			catch (std::invalid_argument iaex)  // �߸��� ���� ������ ���� ó���� �Լ�.
			{
				std::cout << "error in read bvh file: " << buffer[i] << std::endl;
			}

		}


		for (unsigned int i = 0; i < (int)(res_xyz.size()/3); i++)  // res_xyz�� ����� Rx,Ry,Rz �����͸� ���� ������ ��ȯ
		{
			res.push_back((int)(sqrt(res_xyz[3*i]* res_xyz[3 * i]+ res_xyz[3 * i+1] * res_xyz[3 * i+1]+ res_xyz[3 * i+2] * res_xyz[3 * i+2])*10));
		}

		buffer.clear();
		vector<string>(buffer).swap(buffer); // �޸� ����
		res_xyz.clear();
		vector<double>(res_xyz).swap(res_xyz); // �޸� ����
		return res;

	}

public:
	string model_name;
	string name;
	string package;
	int n_frame = 0;
	double t_frame = 0;
	vector< vector<int> > joint_pos; // �Ҽ��� 1°¥��[10��]���� ������ ���� ����
	vector< string > joint_name;

	Bvh()
	{
	}

	~Bvh()
	{

		// joint_pos  �޸� ����
		for (unsigned int i = 0; i < joint_pos.size(); i++)
		{
			joint_pos[i].clear();
			vector<int>(joint_pos[i]).swap(joint_pos[i]);
		}

		joint_pos.clear();
		vector< vector<int> >(joint_pos).swap(joint_pos);

		joint_name.clear();
		vector< string >(joint_name).swap(joint_name);

	}


	int read(string base_path, string new_package, string new_name)
	{	
		string path = base_path + new_package + "/bvh/" + new_name;
		package = new_package;
		name = new_name;
		cout << "open bvh:"<<path << endl;
		ifstream file(path);
		string buffer;
		

		if (file.is_open())
		{
			// [1]  1�� "HIERARCHY" �� �����ϴ��� �˻� 
			getline(file, buffer);
			if (buffer.find("HIERARCHY") == std::string::npos)
			{
				cout << "line 1 HIERARCHY not found, exit!" << endl; // ���н� ����
				return -1;
			}

			// [2]  2�� "ROOT ~" ���� ~ �κ��� ���̸��� ����.
			getline(file, buffer); 
			if ((buffer.size()) > 5)
			{
				model_name = buffer.substr(5); 
			}

			// [3]  3~n ��. JOINT �̸��� ������ ����. 
			getline(file, buffer);
			while (buffer.find("MOTION") == std::string::npos) // n�ٿ��� "MOTION\n"�� ������ ����
			{
				int pos = 0;
				if (pos = buffer.find("JOINT") != std::string::npos) // �ٿ� "JOINT" �� ������ �߰� 
				{
					joint_name.push_back(buffer.substr(pos));
				}
				getline(file, buffer);
			}
			// [4] n+1�� "Frames:"
			getline(file, buffer);
			int pos = 0;
			if (pos = buffer.find("Frames: ") == std::string::npos)
			{
				cout << "Frames not found, exit!" << endl; // ���н� ����
				return -1;
			}
			n_frame = stoi(buffer.substr(pos+8));

			// [5] n+2�� "Frame Time:"
			getline(file, buffer);
			pos = 0;
			if (pos = buffer.find("Frame Time: ") == std::string::npos)
			{
				cout << "Frame Time not found, exit!" << endl; // ���н� ����
				return -1;
			}
			t_frame = ::atof(buffer.substr(pos+12).c_str());
			

			// [6]  �����Ӻ� Rx,Ry,Rz ������, 
			for (int i = 0; i < n_frame; i++) 
			{
				getline(file, buffer);
				joint_pos.push_back(split_double(buffer));
			}
			file.close();
		}
		else
		{
			cout << "can't find file" << endl;
		}
	}
};

class PCA9685
{
private:
	int fd;
	int address = 0x40;
	int freq = 300;
	const int PIN_MIN = 800;
	const int PIN_MAX = 3000;
	const int MAX_PWM = 4096;
	const int LED0_ON_L =0x6;
	const int LEDALL_ON_L =0xFA;
	const int PIN_ALL =16;
	const int PCA9685_MODE1 =0x0;
	const int PCA9685_PRESCALE =0xFE;
	float factor = (float)(PIN_MAX - PIN_MIN) / 170;
	int angle_preset[16] = { 0 };
	int baseReg(int pin)
	{
		return (pin >= PIN_ALL ? LEDALL_ON_L : LED0_ON_L + 4 * pin);
	}

	int setup()
	{

		//- 1.set -------------
		// Check i2c address
		fd = wiringPiI2CSetup(address);
		if (fd < 0)
			return -1;

		// Setup the chip. Enable auto-increment of registers.
		int settings = wiringPiI2CReadReg8(fd, PCA9685_MODE1) & 0x7F;
		int autoInc = settings | 0x20;

		wiringPiI2CWriteReg8(fd, PCA9685_MODE1, autoInc);

		//- 2.reset -------------
		wiringPiI2CWriteReg16(fd, LEDALL_ON_L, 0x0);
		wiringPiI2CWriteReg16(fd, LEDALL_ON_L + 2, 0x1000);

		//- 3.set frequency -------------
		setPWMFreq();
		return 0;
	}

	void setPWMFreq()
	{
		// Cap at min and max
		freq = (freq > 1000 ? 1000 : (freq < 40 ? 40 : freq));

		// To set pwm frequency we have to set the prescale register. The formula is:
		// prescale = round(osc_clock / (4096 * frequency))) - 1 where osc_clock = 25 MHz
		// Further info here: http://www.nxp.com/documents/data_sheet/PCA9685.pdf Page 24
		int prescale = (int)(25000000.0f / (4096 * freq) - 0.5f);

		// Get settings and calc bytes for the different states.
		int settings = wiringPiI2CReadReg8(fd, PCA9685_MODE1) & 0x7F;	// Set restart bit to 0
		int sleep = settings | 0x10;									// Set sleep bit to 1
		int wake = settings & 0xEF;									// Set sleep bit to 0
		int restart = wake | 0x80;										// Set restart bit to 1

		// Go to sleep, set prescale and wake up again.
		wiringPiI2CWriteReg8(fd, PCA9685_MODE1, sleep);
		wiringPiI2CWriteReg8(fd, PCA9685_PRESCALE, prescale);
		wiringPiI2CWriteReg8(fd, PCA9685_MODE1, wake);

		// Now wait a millisecond until oscillator finished stabilizing and restart PWM.
		delay(1);
		wiringPiI2CWriteReg8(fd, PCA9685_MODE1, restart);
	}

public:

	PCA9685(int new_address)
	{
		address = new_address;

		if (setup() != 0)
		{
			cout << "ERRor in I2C SETUP" << endl;
		}
		else
		{
			for(int i =0; i<16; i++)
			{
				setAngle(i,0);
			}
			cout << "set angle to 0" << endl;
			std::this_thread::sleep_for(2);
			for(int i =0; i<16; i++)
			{
				setAngle(i,30);
			}
			cout << "set angle to 30" << endl;
			std::this_thread::sleep_for(2);
			for(int i =0; i<16; i++)
			{
				setAngle(i,60);
			}
			cout << "set angle to 60" << endl;
			std::this_thread::sleep_for(2);
			for(int i =0; i<16; i++)
			{
				setAngle(i,90);
			}
			cout << "set angle to 90" << endl;
			std::this_thread::sleep_for(2);
			for(int i =0; i<16; i++)
			{
				setAngle(i,120);
			}
			cout << "set angle to 120" << endl;
			std::this_thread::sleep_for(2);
		}
		
	}

	~PCA9685() {

	}


	void setAngle(int pin, int angle)
	{
		if ((pin < 0) || pin > 15) {
			return;
		}
		angle += angle_preset[pin];
		angle = (angle > 170 ? 170 : (angle < 0 ? 0 : angle));

		int reg = baseReg(pin);
		int off = PIN_MIN + angle * factor;
		wiringPiI2CWriteReg16(fd, reg, 0 & 0x0FFF);
		wiringPiI2CWriteReg16(fd, reg + 2, off & 0x0FFF);
	}

};

class Controller 
{
private:
	PCA9685 PCA9685_0;
	string base_path = "/home/rpi/catkin_ws/sc_engine/package/";
	chrono::high_resolution_clock::time_point us_clock;
	chrono::high_resolution_clock::time_point run_clock;
	int mapping_table[22] = { 0 };
	int map_joints() 
	// bvh������ ���������� �ϵ������ ���� ������ ����, �ϵ��ڵ�, 
	//�Ŀ� ���� ���Ϸκ��� �а� ��������  
	{
		/* 
		0: HIERARCHY [����]		1: ROOT Base [����]		2: B_rotate [���̽� ȸ��,�߰�����]		3:B_height [���̽� ����, �߰�����]
		4: connect [����]		5: B		[����]		6:  head : �Ӹ�����...

		�κ� ���� �Ϸ���, ������ �� �ۼ��� �� ��������
		
		*/
		for (int i = 0; i < 16; i++) 
		{
			/*
			0->6;
			1->7;
			'''
			15->21;
			*/
			mapping_table[i] = i + 6;
		}
		for (int i = 16; i < 22; i++)
		{
			mapping_table[i] = -1;
		}
		return 0;
	}

	vector<string> getdir(string package) // package ������ bvh ���ϸ���Ʈ�� �о���� �Լ� 
	{
		string bvh_path = base_path + package +"/bvh";
		DIR *dp;
		vector<string>  new_Bvh_names; // ���� ���� bvh �����̸��� ���Ϳ� ����.
		struct dirent *dirp;
		if ((dp = opendir(bvh_path.c_str())) == NULL)
		{
			cout << "Error(" << errno << ") opening " << bvh_path << endl;
			return new_Bvh_names; // �б� ���н� �� ���͸� ����, �ۿ��� ó��.
		}

		while ((dirp = readdir(dp)) != NULL)
		{
			string name = string(dirp->d_name);
			if (name.find(".bvh") != -1) // Ȯ���� �˻�.
			{
				new_Bvh_names.push_back(name);
			}
		}
		closedir(dp);
		return new_Bvh_names; // �̸��� ��� ���͸� ����
	}

	int find_motion(string  package, string motion_name) // package ,motion_name�� ��ġ�ϴ� motion�� ã�� �Լ� 
	{
		vector<Bvh>::iterator ptr = find_if(Bvh_datas.begin(), Bvh_datas.end(),
			[&package,&motion_name](const Bvh& i) {return (i.package == package) && (i.name  == motion_name);}); 
		// �����Լ��� �˻�. 

		if (ptr != Bvh_datas.end())
		{
			int index = distance(Bvh_datas.begin(), ptr);
			cout << "find motion :" << motion_name << " at :" << index << endl;
			return index;
		}
		else
		{
			cout << "can't find motion :" << motion_name << endl;
			return -1;
		}
	}

	double get_duration_us(chrono::high_resolution_clock::time_point t1) // us ������ ���ؽð� t1�� ���ݻ����� ����ð��� ����.
	{
		chrono::high_resolution_clock::time_point now = chrono::high_resolution_clock::now();
		chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double>>(now - t1);
		return (time_span.count() * 1000000);
	}


public:
	vector<Bvh> Bvh_datas;
	Bvh now_motion;
	bool stop_motion = FALSE;
	Controller() : PCA9685_0(0x40)
	{
		map_joints();
	}
	~Controller() {}

	int read_Bvhs(string package)
	{
		vector<string> new_Bvh_names = getdir(package);
		vector<Bvh> new_Bvh_datas;
		if (new_Bvh_names.size() == 0) // ��Ű�� �ȿ� ������ �ϳ��� ������
		{
			cout << "there is no Motion file on "<< package<< " exit read!"<<endl;
			return 0;
		}

		for (unsigned int i = 0; i < new_Bvh_names.size(); i++)
		{
			Bvh new_Bvh;
			try
			{
				new_Bvh.read(base_path, package , new_Bvh_names[i]);
			}
			catch (const ifstream::failure& e) {
				cout << "Exception opening/reading file " << new_Bvh_names[i] << "set motion to zero" << endl;
			}
			catch (...)
			{
				cout << "Exception someting.. " << new_Bvh_names[i] << "set motion to zero" << endl;
			}
			new_Bvh_datas.push_back(new_Bvh);
		}
		Bvh_datas.insert(Bvh_datas.end(), new_Bvh_datas.begin(), new_Bvh_datas.end());
		new_Bvh_names.clear();
		vector< string >(new_Bvh_names).swap(new_Bvh_names);
		return 0;
	}

	void print_Bvhs() 
	{
		for (unsigned int i = 0; i < Bvh_datas.size(); i++)
		{
			cout << Bvh_datas[i].package << " : " << Bvh_datas[i].name << endl;
		}
	}


	void run_Bvh(string package, string motion_name) 
	{	
		
		int index = find_motion(package, motion_name);
		if (index == -1)
		{
			cout << "there is no motion.. " << package << " : " << motion_name << endl;
			return;
		}
		else 
		{	
			
			now_motion = Bvh_datas[index];
			us_clock = chrono::high_resolution_clock::now();

			std::promise<void> exitSignal;
			auto future = std::async(launch::async,&Controller::ctr_thread, this);

			std::chrono::milliseconds span(100);
			while (future.wait_for(span) == std::future_status::timeout)
			{
				double total_time = get_duration_us(us_clock) / 1000000;
				cout << "run :" << total_time << "s"<<endl;
			}

			auto result = future.get();
			double total_time = get_duration_us(us_clock)/1000000;
		    cout << "end trajectory" << package << " : " << motion_name<< " est :"<<now_motion.n_frame*now_motion.t_frame << "s  real:"<<total_time << endl;
		}
	}

	
	int ctr_thread()
	{	
		//vector<int> real;
		//vector<int> tar;
		run_clock = chrono::high_resolution_clock::now();
		for (int frame = 0; frame < now_motion.n_frame; frame++) 
		{	
			if (stop_motion)
				return -1;

			for (int joint = 0; joint < 22; joint++) 
			{
				int motor_index = mapping_table[joint];
				if (motor_index != -1) 
				{
					PCA9685_0.setAngle(motor_index, round(now_motion.joint_pos[frame][joint] / 10));
				}
			}
			int now_time = (int)get_duration_us(run_clock);
			int target_time = (int)((frame+1) * now_motion.t_frame * 1000000);
			//tar.push_back(target_time);
			//real.push_back(now_time);
			usleep(abs(target_time - now_time));
		}
		/*
		for (int n = 0; n < tar.size() ; n++)
		{
			cout << n <<" frame real:" << real[n] << "us  tar: " << tar[n]<<"us"<< endl;
		}
		*/
		return 0;
	}


};

int Rand_ ( int min, int max )
{
    random_device rn;
    mt19937_64 rnd( rn() );
    uniform_int_distribution<int> range(min, max);
 
    return range( rnd );
}



int main(int argc, char*argv[])
{
	std::random_device rd;


	cout << "hello world" << endl;
	Controller Ctr1;
	Ctr1.read_Bvhs("0");


	//Ctr1.run_Bvh("0", "ts2.bvh");
	while(1)
	{
		int sel_motion = Rand_(0,2);
		if (sel_motion == 0) 
		{
			Ctr1.run_Bvh("0","0.bvh");
		}
		else if (sel_motion == 1) 
		{
			Ctr1.run_Bvh("0","1.bvh");
		}
		else if (sel_motion == 2) 
		{
			Ctr1.run_Bvh("0","2.bvh");
		}
		else 
		{
			 std::this_thread::sleep_for (std::chrono::seconds( Rand_(0,2)));	
		}
	}
	return 0;

}


